from gasp import games
from gasp import color
from gasp import boards
from random import randint

BOX_SIZE = 60
MARGIN = 40
WIDTH = 3
HEIGHT = WIDTH
TEXT_SIZE = 18
NEXT_PLAYER = {1 : 2, 2 : 1}
SHAPES = {1 : 'X', 2 : 'O'}

class Game(boards.SingleBoard):
    def __init__(self):
        self.init_singleboard((MARGIN, MARGIN), WIDTH, HEIGHT, BOX_SIZE)
        self.draw_all_outlines()
        self.current_player = 1
        self.tie = 0
        self.game_over = 0
        self.moves = []
        self.handle = open('/Users/1030365/Projects/csc-200/AI/tictactoedata.txt','r+')
        self.data = {}
        for line in self.handle:
            strg = ''
            for char in line:
                if char == '@' or char == '^':
                    continue
                strg+=char
            self.data[strg[:(WIDTH*HEIGHT)]] = int(strg[(WIDTH*HEIGHT+1):])
        self.handle.close()
        self.handle = open('/Users/1030365/Projects/csc-200/AI/tictactoedata.txt','r+')
        if randint(0,1)==0:
            self.bot_turn()


    def new_gamecell(self, i, j):
        return Square(self, i, j)

    def mouse_up(self, xy, button):
        x, y = xy[0], xy[1]
        i, j = self.coords_to_cell(x, y)
        if self.on_board(i, j) and self.game_over == 0:
            if self.grid[i][j].owner == 0:
                self.grid[i][j].capture(self.current_player)
                self.check_game()
                if self.game_over == 0:
                    self.current_player = NEXT_PLAYER[self.current_player]
                    self.bot_turn()
                elif self.tie == 0:
                    self.learn(-1)

    def learn(self, winner):
        for state in self.moves:
            self.data[state] = self.data.get(state, 0)+winner
        self.handle.truncate(0)
        for state in self.data.keys():
            self.handle.write(f'{state} {self.data[state]}\n')
        self.handle.close()

    def bot_turn(self):
        choices = []
        penalty = 0
        for state in self.possible():
            self.data[state] = self.data.get(state, 0)
            if self.data[state] < penalty:
                penalty = self.data[state]
        penalty*=-1
        for state in self.possible():
            strg = 0
            boost = 0
            if self.data[state] < 0:
                strg = self.data[state]*-1
            else:
                boost = 1
            for i in range(0, (self.data[state]+strg+1)*(boost*penalty)+1):
                choices.append(state)
        while True:
            i, j = self.statecoords(choices[randint(0,len(choices)-1)])
            if self.on_board(i, j) and self.game_over == 0:
                if self.grid[i][j].owner == 0:
                    self.grid[i][j].capture(self.current_player)
                    self.check_game()
                    self.moves.append(self.encode())
                    if self.game_over == 0:
                        self.current_player = NEXT_PLAYER[self.current_player]
                    else:
                        self.learn(1)
                    break

    def check_game(self):
        for i in range(0, WIDTH):
            strg = []
            for j in range(0, HEIGHT):
                strg.append(self.grid[i][j].owner)
            if strg == [self.current_player]*HEIGHT:
                self.game_over = 1
                return None
        for j in range(0, HEIGHT):
            strg = []
            for i in range(0, WIDTH):
                strg.append(self.grid[i][j].owner)
            if strg == [self.current_player]*WIDTH:
                self.game_over = 1
                return None
        strg = []
        for i in range(0, WIDTH):
            strg.append(self.grid[i][i].owner)
        if strg == [self.current_player]*WIDTH:
            self.game_over = 1
            return None
        strg = []
        for i in range(0, WIDTH):
            strg.append(self.grid[i][HEIGHT-i-1].owner)
        if strg == [self.current_player]*WIDTH:
            self.game_over = 1
            return None
        strg = 1
        for i in range(0, WIDTH):
            for j in range(0, HEIGHT):
                if self.grid[i][j].owner == 0:
                    strg = 0
        if strg == 1:
            self.tie = 1
            self.game_over = 1

    def encode(self):
        strg = ''
        for j in range(0, HEIGHT):
            for i in range(0, WIDTH):
                strg+=str(self.grid[i][j].owner)
        return strg

    def possible(self):
        strg = []
        for tile in range(0,len(self.encode())):
            if self.encode()[tile] == '0':
                strg.append(self.encode()[:tile]+str(self.current_player)+self.encode()[(tile+1):])
        return strg

    def statecoords(self, state):
        for j in range(0, HEIGHT):
            for i in range(0, WIDTH):
                if state[(WIDTH*j)+i] == self.encode()[(WIDTH*j)+i]:
                    continue
                return (i, j)


class Square(boards.GameCell):
    def __init__(self, board, i, j):
        self.init_gamecell(board, i, j)
        self.owner = 0
        self.x, self.y = board.cell_to_coords(i, j)
        self.board = board

    def capture(self, player):
        self.owner = player
        self.shape = games.Text(self.board, self.x+BOX_SIZE/2, self.y+BOX_SIZE/2, SHAPES[player], BOX_SIZE, color.BLACK)
        

game = Game()
game.mainloop()
