import random
from random import randint



global data, moves, next_player

def vertical(grid):
    strga=[]
    strgb=''
    for i in range(0,3):
        for z in range(0,3):
            strgb=strgb+grid[i+(3*z)]
        strga.append(strgb)
        strgb=''
    return strga

def horizontal(grid):
    strga=[]
    for i in range(0,3):
        strga.append(grid[(3*i):(3*i+3)])
    return strga

def diagonal(grid):
    return [grid[0]+grid[4]+grid[8],grid[2]+grid[4]+grid[6]]

def display(grid):
    grid=grid.replace('0',' ')
    grid=grid.replace('1','X')
    grid=grid.replace('2','O')
    print(f'|{grid[:3]}|\n|{grid[3:6]}|\n|{grid[6:]}|')

def win(grid,p):    
    for i in horizontal(grid):
        if i==f'{p}{p}{p}':
            return True
    for i in vertical(grid):
        if i==f'{p}{p}{p}':
            return True
    for i in diagonal(grid):
        if i==f'{p}{p}{p}':
            return True
    return False

def turn(grid):
    xcount=0
    ocount=0
    for i in grid:
        if i=='1':
            xcount+=1
        elif i=='2':
            ocount+=1
    if xcount==ocount:
        return 1
    return 2

def winmove(grid):
    p=turn(grid)
    return check(grid,p)

def block(grid):
    if turn(grid)==1:
        p=2
    else:
        p=1
    return check(grid,p)
    
def check(grid,p):    
    strg=0
    for i in horizontal(grid):
        if i==f'0{p}{p}':
            return 3*strg
        if i==f'{p}0{p}':
            return 3*strg+1
        if i==f'{p}{p}0':
            return 3*strg+2
        strg+=1
    strg=0
    for i in vertical(grid):
        if i==f'0{p}{p}':
            return strg
        if i==f'{p}0{p}':
            return 3+strg
        if i==f'{p}{p}0':
            return 6+strg
        strg+=1
    strg=0
    for i in diagonal(grid):
        if i==f'0{p}{p}':
            return 2*strg
        if i==f'{p}0{p}':
            return 4
        if i==f'{p}{p}0':
            return 8-(2*strg)
        strg+=1
    return '?'

def move(grid):
    if winmove(grid)=='?':
        if block=='?':
            return '?'
        return block(grid)
    return winmove(grid)

def options(grid):
    if move(grid)=='?':
        strg=[]
        for i in range(0,9):
            if grid[i]=='0':
                strg.append(i)
        return strg
    return [move(grid)]

def possible(grid):
    strg = []
    for tile in range(0,len(grid)):
        if grid[tile] == '0':
            strg.append(grid[:tile]+str(turn(grid))+grid[(tile+1):])
    return strg

def statecoords(grid, state):
    for j in range(0, 3):
        for i in range(0, 3):
            if state[(3*j)+i] == grid[(3*j)+i]:
                continue
            return (i, j)



def play(grid):
    global data, moves
    choices = []
    penalty = 0
    for state in possible(grid):
        data[state] = data.get(state, 0)
        if data[state] < penalty:
            penalty = data[state]
    penalty*=-1
    for state in possible(grid):
        strg = 0
        boost = 0
        if data[state] < 0:
            strg = data[state]*-1
        else:
            boost = 1
        for i in range(0, (data[state]+strg+1)*(boost*penalty)+1):
            choices.append(state)
    while True:
        strg = choices[randint(0,len(choices)-1)]
        moves[turn(grid)-1].append(strg)
        return strg

def learn(bank, winner):
    global moves, data, next_player
    for state in moves[winner-1]:
        data[state] = data.get(state, 0)+1
    for state in moves[next_player[winner]-1]:
        data[state] = data.get(state, 0)-1
    bank.truncate(0)
    for state in data.keys():
        bank.write(f'{state} {data[state]}\n')

wins = {1 : 0, 2 : 0}
for game in range(0,1000):
    moves = [[], []]

    handle = open('/Users/1030365/Projects/csc-200/AI/tictactoedata.txt')
    data = {}
    for line in handle:
        strg = ''
        for char in line:
            strg+=char
        data[strg[:(3*3)]] = int(strg[(3*3+1):])
    handle.close()
    handle = open('/Users/1030365/Projects/csc-200/AI/tictactoedata.txt','r+')

    next_player = {1 : 2, 2 : 1}
    board='000000000'

    strg = ''
    winner=0

    for i in range(0,9):
        board = play(board)
        if win(board, next_player[turn(board)]):
            winner = next_player[turn(board)]
            break
    if winner==0:
        print('it\'s a tie')
    else:
        learn(handle,winner)
        wins[winner]=wins[winner]+1
        print(f'player {winner} wins!')
    handle.close()
print(f'player 1 won {wins[1]} times, player 2 won {wins[2]} times.')
