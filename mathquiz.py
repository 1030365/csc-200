from gasp import *
from random import randint

#print(f'{num1} x {num2} = {str(int(num1)*int(num2))}, your answer was {input(f"{num1} x {num2} = ")}.')
#print((f'{num3}\n'*5)[:-1])
'''if int(input('Type a number: '))==100:
    print('Fun Fact: the number you wrote is a 4 in binary.')
else:
    print('Congrats! You\'ve typed one of the infinite numbers that isn\'t 100!')'''
right=0
for i in range(0,10):
    (num1,num2,num3)=(randint(1,10),randint(1,10),randint(1,10))
    if input(f'What\'s {num1} times {num2}? ')==str(num1*num2):
        print('That\'s right - well done.')
        right+=1
    else:
        print(f'No, I\'m afraid the answer is {num1*num2}.')
print(f'\nI asked you 10 questions. You got {right} of them right.')
print('Well done!')
