import random
from random import randint

def odds(nume,denom):
    x=[]
    for i in range(0,10000):
        count=0
        while True:
            count+=1
            if randint(nume,denom)==nume:
                x.append(count)
                break
    return sum(x)/len(x)
