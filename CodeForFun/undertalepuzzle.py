from gasp import games
from gasp import boards
from gasp import color
from random import randint

WIDTH = 20
HEIGHT = 6
BOX_SIZE = 40
MARGIN = 60
COLORS = [color.GRAY, color.ORANGE, color.PINK, color.RED, color.YELLOW, color.LIGHTGREEN, color.BLUE, color.PURPLE]

class Game(boards.SingleBoard):
    def __init__(self):
        self.init_singleboard((MARGIN, MARGIN), WIDTH+2, HEIGHT, BOX_SIZE)
        self.draw_all_outlines()
        self.ghost = Ghost(self)
        self.player = Player(self)
        self.orange = False
        for i in range(1, WIDTH+1):
            for j in range(0, HEIGHT):
                if self.grid[i][j].effect == 4:
                    self.grid[i][j].zap()
        self.direction = None
        self.simorange = False
        while True:
            if self.ghost.i == 0:
                self.direction = boards.K_RIGHT
            if self.direction == None:
                strg = []
                if self.on_board(self.ghost.i+1, self.ghost.j):
                    if not self.grid[self.ghost.i+1][self.ghost.j].solution:
                        strg.append(boards.K_RIGHT)
                if self.on_board(self.ghost.i-1, self.ghost.j) and self.ghost.i !=1:
                    if not self.grid[self.ghost.i-1][self.ghost.j].solution:
                        strg.append(boards.K_LEFT)
                if self.on_board(self.ghost.i, self.ghost.j+1) and self.ghost.j < HEIGHT-1:
                    if not self.grid[self.ghost.i][self.ghost.j+1].solution:
                        strg.append(boards.K_DOWN)
                if self.on_board(self.ghost.i, self.ghost.j-1) and self.ghost.j > 0:
                    if not self.grid[self.ghost.i][self.ghost.j-1].solution:
                        strg.append(boards.K_UP)
                if len(strg) > 0:
                    self.direction = self.chooser(strg)
                else:
                    self.direction = self.chooser([boards.K_UP, boards.K_DOWN, boards.K_LEFT, boards.K_RIGHT])
            self.ghost.moving(self.direction)
            if self.ghost.winner():
                self.orange = False
                break
            if self.ghost.i == 0:
                continue
            thecolor = self.chooser([1,1,2,2,5,5,7,6,6])
            if self.simorange:
                thecolor = self.chooser([2,2,5,5,7,6,6])
            self.grid[self.ghost.i][self.ghost.j].become(thecolor)
            if thecolor == 1:
                self.simorange = True
            elif thecolor == 7:
                self.simorange = False
            self.grid[self.ghost.i][self.ghost.j].solution = True
            if self.grid[self.ghost.i][self.ghost.j].effect != 7:
                self.direction = None
        self.reboot()

    def reboot(self):
        for i in range(1, WIDTH+1):
            for j in range(0, HEIGHT):
                if self.grid[i][j].effect == 6:
                    for coord in [(1,0),(-1,0),(0,1),(0,-1)]:
                        if not self.on_board(i+coord[0],j+coord[1]) or i+coord[0]==0:
                            continue
                        if self.grid[i+coord[0]][j+coord[1]].effect == 0:
                            self.grid[i+coord[0]][j+coord[1]].become(self.chooser([1,2,3,5,6,7]))
        for i in range(1, WIDTH+1):
            for j in range(0, HEIGHT):
                if self.grid[i][j].effect == 0:
                    self.grid[i][j].become(randint(1,7))
        self.orange = False
        for i in range(1, WIDTH+1):
            for j in range(0, HEIGHT):
                if self.grid[i][j].effect == 4:
                    self.grid[i][j].zap()
        for i in range(1, WIDTH+1):
            for j in range(0, HEIGHT):
                if self.grid[i][j].electric or self.grid[i][j].effect == 3:
                    continue
                self.grid[i][j].walkable = True
        for j in range(0, HEIGHT):
            self.grid[WIDTH+1][j].become(0)

    def chooser(self, thelist):
        return thelist[randint(0, len(thelist)-1)]

    def keypress(self, key):
        self.player.moving(key)

    def new_gamecell(self, i, j):
        return Tile(self, i, j)

class Tile(boards.GameCell):
    def __init__(self, board, i, j):
        self.init_gamecell(board, i, j)
        self.color = COLORS[0]
        self.walkable = True
        self.effect = 0
        self.i = i
        self.j = j
        self.solution = False
        self.electric = False
        if i == 0 and j != 0:
            self.color = color.BLACK
        elif i < WIDTH+1 and i > 0:
            self.effect = 0
            self.color = COLORS[self.effect]
        self.set_color(self.color)
        if self.effect == 3:
            self.walkable = False
        if self.effect == 7:
            self.board.orange = False

    def zap(self):
        self.electric = True
        self.walkable = False
        for i in range(-1, 2):
            if self.board.on_board(self.i+i,self.j):
                tile = self.board.grid[self.i+i][self.j]
                if tile.effect == 6 and not tile.electric:
                    tile.zap()
        for j in range(-1, 2):
            if self.board.on_board(self.i,self.j+j):
                tile = self.board.grid[self.i][self.j+j]
                if tile.effect == 6 and not tile.electric:
                    tile.zap()

    def become(self, blocktype):
        self.effect = blocktype
        self.color = COLORS[self.effect]
        self.set_color(self.color)
        if self.effect == 3:
            self.walkable = False

class Ghost:
    def __init__(self, board):
        self.board = board
        self.i, self.j = 0, 0

    def canwalk(self, row, col):
        if not self.board.on_board(self.i+row,self.j+col):
            return False
        if self.i+row == 0:
            if self.j+col == 0:
                return True
            return False
        if self.board.grid[self.i][self.j].effect == 7 and self.board.grid[self.i+row][self.j+col].effect == 6:
            if not self.board.grid[self.i+row][self.j+col].electric:
                return True
        if not self.board.grid[self.i+row][self.j+col].walkable:
            return False
        return True

    def moving(self, key):
        slip = False
        if self.board.grid[self.i][self.j].effect == 7:
            self.board.orange = False
        if key == boards.K_UP and self.canwalk(0,-1):
            self.j -= 1
            if not self.canwalk(0,-1):
                self.board.direction = None
        elif key == boards.K_DOWN and self.i > 0 and self.canwalk(0,1):
            self.j += 1
            if not self.canwalk(0,1):
                self.board.direction = None
        elif key == boards.K_RIGHT and self.canwalk(1,0):
            self.i += 1
            if not self.canwalk(1,0):
                self.board.direction = None
        elif key == boards.K_LEFT and self.canwalk(-1,0):
            self.i -= 1
            if not self.canwalk(-1,0):
                self.board.direction = None
        else:
            self.board.direction = None
        if self.j < 0:
            self.j += 1
        elif self.j == HEIGHT:
            self.j -= 1
        if self.i == 0 and self.j != 0:
            self.i = 1
        elif self.i == WIDTH+2:
            self.i = WIDTH+1
        if self.board.grid[self.i][self.j].effect == 1:
            self.board.orange = True
        elif self.board.grid[self.i][self.j].effect == 7:
            self.board.orange = False
        for i in range(1, WIDTH+1):
            for j in range(0, HEIGHT):
                if self.board.grid[i][j].effect == 6 and not self.board.grid[i][j].electric:
                    if self.board.orange:
                        self.board.grid[i][j].walkable = False
                    else:
                        self.board.grid[i][j].walkable = True
        if slip and self.board.grid[self.i][self.j].effect == 7:
            self.moving(key)

    def winner(self):
        return self.i == WIDTH+1

class Player(games.Circle):
    def __init__(self, board):
        self.board = board
        self.i, self.j = 0, 0
        x, y = self.board.cell_to_coords(self.i, self.j)
        x += BOX_SIZE/2
        y += BOX_SIZE/2
        self.init_circle(board, x, y, (BOX_SIZE*2)/5, color.BLACK)

    def moving(self, key):
        slip = False
        if self.board.grid[self.i][self.j].effect == 7:
            self.board.orange = False
        if key == boards.K_UP and self.canwalk(0,-1):
            self.j -= 1
            if self.canwalk(0,-1):
                slip = True
        if key == boards.K_DOWN and self.i > 0 and self.canwalk(0,1):
            self.j += 1
            if self.canwalk(0,1):
                slip = True
        if key == boards.K_RIGHT and self.canwalk(1,0):
            self.i += 1
            if self.canwalk(1,0):
                slip = True
        if key == boards.K_LEFT and self.canwalk(-1,0):
            self.i -= 1
            if self.canwalk(-1,0):
                slip = True
        if self.j < 0:
            self.j += 1
        elif self.j == HEIGHT:
            self.j -= 1
        if self.i == 0 and self.j != 0:
            self.i = 1
        elif self.i == WIDTH+2:
            self.i = WIDTH+1
        x, y = self.board.cell_to_coords(self.i, self.j)
        x+=BOX_SIZE/2
        y+=BOX_SIZE/2
        self.destroy()
        self.init_circle(self.board, x, y, (BOX_SIZE*2)/5, color.BLACK)
        if self.board.grid[self.i][self.j].effect == 1:
            self.board.orange = True
        elif self.board.grid[self.i][self.j].effect == 7:
            self.board.orange = False
        for i in range(1, WIDTH+1):
            for j in range(0, HEIGHT):
                if self.board.grid[i][j].effect == 6 and not self.board.grid[i][j].electric:
                    if self.board.orange:
                        self.board.grid[i][j].walkable = False
                    else:
                        self.board.grid[i][j].walkable = True
        if slip and self.board.grid[self.i][self.j].effect == 7:
            self.moving(key)

    def canwalk(self, row, col):
        if not self.board.on_board(self.i+row,self.j+col):
            return False
        if self.board.grid[self.i][self.j].effect == 7 and self.board.grid[self.i+row][self.j+col].effect == 6:
            if not self.board.grid[self.i+row][self.j+col].electric:
                return True
        if not self.board.grid[self.i+row][self.j+col].walkable:
            return False
        return True

game = Game()
game.mainloop()
