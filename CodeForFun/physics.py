from gasp import *
import threading

begin_graphics()

class Player:
    x=1
    y=1
    pos=(100+x,100+y)
    shape=Box(pos,10,10)
    xmentum=0
    ymentum=0
    def move(self,key):
        if key=='a':
            self.xmentum-=10
        elif key=='d':
            self.xmentum+=10


class Terrain:
    def __init__(self,xone,yone,xtwo,ytwo):
        (self.xone,self.yone,self.xtwo,self.ytwo)=(xone,yone,xtwo,ytwo)
        self.shape=Box((xone,yone),xtwo-xone,ytwo-yone,filled=True)

class Background:
    def bground():
        while True:
            Player.x=Player.x+Player.xmentum
            try:
                Player.xmentum-=(Player.xmentum/abs(Player.xmentum))
            except:
                pass
            move_to(Engine.player.shape,(Engine.player.pos))

class Engine:
    player=Player()
    background=threading.Thread(name='background',target=Background.bground)
    background.start()

game=Engine
game.player.move('d')
while not update_when('key_pressed')=='q':
    pass
end_graphics()
