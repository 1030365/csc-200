import random
from random import randint

def newround(num):
    x=int((str(float(num)).split('.'))[0])
    if num-x==0.5:
        return x+1
    else:
        return round(num)

def snap(names):
    survivors=names
    if newround(len(names)/2)==len(names)/2:
        dust=len(names)/2
    else:
        dust=newround((len(names))/2)-randint(0,1)
    while dust>0:
        strg=[]
        x=randint(0,1)
        for name in survivors:
            if dust>0 and randint(0,1)==1:
                dust-=1
            else:
                strg.append(name)
        survivors=strg
    return survivors

def EndgameSpoilers():
    victims=dict()
    while True:
        suspect=((input('Ask if a person dies in endgame: ')).split())[1]
        if suspect=='Tony':
            break
        victims[suspect.upper()]=victims.get(suspect.upper(),randint(0,1))
        if victims[suspect.upper()]==1:
            print(f'Yes, {suspect} does die in endgame.')
        else:
            print(f'No, {suspect} does not die in endgame.')
