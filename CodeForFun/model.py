from gasp import *

class Dims:
    handle=open(input('What file should we use for your model? '))
    lines=[]
    for line in handle:
        lines.append(line[:-1])
    x=int(((lines[0].split(','))[0])[2:])
    y=int(((lines[0].split(','))[1])[3:])
    z=int(((lines[0].split(','))[2])[3:])
    print(f'x={x}, y={y}, z={z}')

class Player:
    def __init__(self,x,y,z):
        (self.x,self.y,self.z)=(x,y,z)
    def show(self,xpos,ypos):
        self.shape=Circle((105+10*xpos,105+10*ypos),5,filled=True,color='GREEN')
    def hide(self):
        try:
            remove_from_screen(self.shape)
        except:
            pass
    def move(self,key,view,blocks):
        self.walls=[]
        for block in blocks:
            self.walls.append((block.x,block.y,block.z))
        if view=='1' or view=='2':
            if key=='w' and not (self.x,self.y+1,self.z) in self.walls:
                self.y+=1
            elif key=='s' and not (self.x,self.y-1,self.z) in self.walls:
                self.y-=1
        if view=='1' or view=='3':
            if key=='d' and not (self.x+1,self.y,self.z) in self.walls:
                self.x+=1
            elif key=='a' and not (self.x-1,self.y,self.z) in self.walls:
                self.x-=1
        if view=='2':
            if key=='d' and not (self.x,self.y,self.z-1) in self.walls:
                self.z-=1
            elif key=='a' and not (self.x,self.y,self.z+1) in self.walls:
                self.z+=1
            move_to(self.shape,(100+10*(dims.z-self.z+1),100+10*self.y))
        if view=='3':
            if key=='w' and not (self.x,self.y,self.z-1) in self.walls:
                self.z-=1
            elif key=='s' and not (self.x,self.y,self.z+1) in self.walls:
                self.z+=1
            move_to(self.shape,(100+10*self.x,100+10*(dims.z-self.z+1)))
        if view=='1':
            move_to(self.shape,(100+10*self.x,100+10*self.y))
class Block:
    def __init__(self,x,y,z):
        self.x=x
        self.y=y
        self.z=z
    def show(self,xpos,ypos):
        self.shape=Box((100+10*xpos,100+10*ypos),10,10,filled=True)
    def hide(self):
        try:
            remove_from_screen(self.shape)
        except:
            pass

class Goal:
    def __init__(self,x,y,z):
        self.x=x
        self.y=y
        self.z=z
    def show(self,xpos,ypos):
        self.shape=Box((100+10*xpos,100+10*ypos),10,10,filled=True,color='BLUE')
    def hide(self):
        try:
            remove_from_screen(self.shape)
        except:
            pass



dims=Dims()
begin_graphics()
blocks=list()
countx=1
county=dims.y
countz=0
for line in dims.lines[1:]:
    if line[-1]==':':
        zcount=int(line[:-1])
        xcount=1
        ycount=dims.y
    else:
        for char in line:
            if char=='#':
                blocks.append(Block(xcount,ycount,zcount))
            elif char=='p':
                player=Player(xcount,ycount,zcount)
            elif char=='g':
                goal=Goal(xcount,ycount,zcount)
            xcount+=1
        ycount-=1
        xcount=1


top=[]
right=[]
front=[]

def insinline(string,dex,char):
    return string[:dex]+char+string[dex+1:]


def graph(coords,xval,yval):
    output=[]
    for coord in coords:
        if not coord in output:
            output.append(coord)
    return output

order=[0,0]
view='0'
key='1'
while True:
    if key in 'wasd':
        player.move(key,view,blocks+[goal])
        key=update_when('key_pressed')
        continue
    view=key
    if key=='1':
        if goal.z<player.z:
            (order[0],order[1])=(goal,player)
        else:
            (order[1],order[0])=(goal,player)
    if key=='2':
        if goal.x<player.x:
            (order[0],order[1])=(goal,player)
        else:
            (order[1],order[0])=(goal,player)
    if key=='3':
        if goal.y<player.y:
            (order[0],order[1])=(goal,player)
        else:
            (order[1],order[0])=(goal,player)
    for block in blocks:
        block.hide()
        if key=='1':
            if block.z<order[0].y:
                block.show(block.x,block.y)
        elif key=='2':
            if block.x<order[0].y:
                block.show(dims.z-block.z+1,block.y)
        elif key=='3':
            if block.y<order[0].y:
                block.show(block.x,dims.z-block.z+1)
    order[0].hide()
    if key=='1':
            order[0].show(order[0].x,order[0].y)
    elif key=='2':
            order[0].show(order[0].z-order[0].z+1,order[0].y)
    elif key=='3':
            order[0].show(order[0].x,dims.z-order[0].z+1)
    for block in blocks:
        if key=='1':
            if block.z>=order[0].z and block.z<order[1].z:
                block.show(block.x,block.y)
        elif key=='2':
            if block.x>=order[0].x and block.x<order[1].x:
                block.show(dims.z-block.z+1,block.y)
        elif key=='3':
            if block.y>=order[0].y and block.y<order[1].y:
                block.show(block.x,dims.z-block.z+1)
    order[1].hide()
    if key=='1':
            order[1].show(order[1].x,order[1].y)
    elif key=='2':
            order[1].show(order[1].z-order[1].z+1,order[1].y)
    elif key=='3':
            order[1].show(order[1].x,dims.z-order[1].z+1)
    for block in blocks:
        if key=='1':
            if block.z>=order[1].z:
                block.show(block.x,block.y)
        elif key=='2':
            if block.x>=order[1].x:
                block.show(dims.z-block.z+1,block.y)
        elif key=='3':
            if block.y>=order[1].y:
                block.show(block.x,dims.z-block.z+1)
    if key=='q':
        break 
    key=update_when('key_pressed')
    if not key in 'wasd':
        for block in blocks:
            block.hide()

end_graphics()
