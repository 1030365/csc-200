from gasp import *
from random import randint

begin_graphics()

class Segment:
    def __init__(self,x,y,segnum):
        self.x=x
        self.y=y
        self.segnum=segnum
        self.shape=Box((x*10,y*10),10,10,filled=True)

    def tailhead(self,x,y):
        move_to(self.shape,(x*10,y*10))
        self.x=x
        self.y=y
        self.segnum=0

    def shiftdown(self):
        self.segnum+=1

class Snake:
    def __init__(self):
        self.segs=[]
        self.length=4
        for i in range(1,self.length+1):
            self.segs.append(Segment(20-i,20,i))
        self.update()

    def update(self):
        self.length=len(self.segs)
        self.tail=self.segs[0]
        self.head=self.segs[0]
        for i in self.segs:
            if i.segnum<self.head.segnum:
                self.head=i
            elif i.segnum>self.tail.segnum:
                self.tail=i
    
    def grow(self,key):
        self.update()
        self.segs.append(Segment(0,0,self.length+1))
        self.regmove(key)

    def regmove(self, key):
        self.update()
        if key=='a':
            self.tail.tailhead(self.head.x-1,self.head.y)
        elif key=='d':
            self.tail.tailhead(self.head.x+1,self.head.y)
        elif key=='w':
            self.tail.tailhead(self.head.x,self.head.y+1)
        elif key=='s':
            self.tail.tailhead(self.head.x,self.head.y-1)
        else:
            return None
        for i in self.segs:
            i.shiftdown()
        self.update()

    def move(self,key,aplx,aply):
        if key=='a':
            if (self.head.x-1,self.head.y)==(aplx,aply):
                self.grow(key)
            else:
                self.regmove(key)
        elif key=='d':
            if (self.head.x+1,self.head.y)==(aplx,aply):
                self.grow(key)
            else:
                self.regmove(key)
        elif key=='w':
            if (self.head.x,self.head.y+1)==(aplx,aply):
                self.grow(key)
            else:
                self.regmove(key)
        elif key=='s':
            if (self.head.x,self.head.y-1)==(aplx,aply):
                self.grow(key)
            else:
                self.regmove(key)


class Apple:
    def __init__(self,xmax,ymax):
        self.xmax=xmax
        self.ymax=ymax
        self.shape=Circle((15,15),5)
        self.x=1
        self.y=1

    def move(self,segs):
        while True:
            self.x=randint(2,self.xmax)
            self.y=randint(2,self.ymax)
            if (self.x,self.y) in segs:
                continue
            break
        move_to(self.shape,(self.x*10+5,self.y*10+5))

class Border:
    def __init__(self,x,y):
        self.x=x
        self.y=y
        self.shape=Box((10*x,10*y),10,10,filled=True,color='BLUE')


class Game:
    def __init__(self):
        self.endgame=0
        self.snake=Snake()
        self.walls=[]
        self.width=60
        self.height=45
        self.apple=Apple(1+self.width,1+self.height)
        self.bords=[]
        for i in range(1,self.height+3):
            self.bords.append(Border(1,i))
            self.bords.append(Border(self.width+2,i))
        for i in range(2,self.width+2):
            self.bords.append(Border(i,1))
            self.bords.append(Border(i,self.height+2))
        self.updategame()
        self.apple.move(self.walls)

    def updategame(self):
        self.walls=[]
        for i in self.bords:
            self.walls.append((i.x,i.y))
        for i in self.snake.segs:
            self.walls.append((i.x,i.y))
        self.count=0
        for i in self.walls:
            if (self.snake.head.x,self.snake.head.y)==i:
                self.count+=1
        if self.count==2:
            self.endgame=1
        if (self.apple.x,self.apple.y)==(self.snake.head.x,self.snake.head.y):
            self.apple.move(self.walls)

    def checksafety(self,key):
        if key=='a' and (not (self.snake.head.x-1,self.snake.head.y) in self.walls):
            return True
        elif key=='d' and (not (self.snake.head.x+1,self.snake.head.y) in self.walls):
            return True
        elif key=='w' and (not (self.snake.head.x,self.snake.head.y+1) in self.walls):
            return True
        elif key=='s' and (not (self.snake.head.x,self.snake.head.y-1) in self.walls):
            return True
        return False

    def run(self):
        while True:
            
            if self.apple.y>self.snake.head.y and self.checksafety('w'):
                self.key='w'
            elif self.apple.y<self.snake.head.y and self.checksafety('s'):
                self.key='s'
            elif self.apple.x<self.snake.head.x and self.checksafety('a'):
                self.key='a'
            elif self.apple.x>self.snake.head.x and self.checksafety('d'):
                self.key='d'
            else:
                self.key=[]
                for i in 'wasd':
                    if self.checksafety(i):
                        self.key.append(i)
                        continue
                    if i=='d' and len(self.key)==0:
                        self.key='a'
                self.key=self.key[randint(0,len(self.key)-1)]

            self.snake.move(self.key,self.apple.x,self.apple.y)
            self.updategame()
            if self.key=='q':
                self.endgame=1
            if self.endgame==1:
                print(f'Score: {self.snake.length}')
                break

game=Game()
game.run()
end_graphics()
