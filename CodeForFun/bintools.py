def binary(num):
    """
      >>> binary(7)
      111
      >>> binary(0)
      0
      >>> binary(75)
      1001011
    """
    x = 0
    count = 0
    if num == 0:
            return 0
    else:
            while 2 ** count <= num:
                    count = count + 1
            count = count - 1
            while count >= 0:
                    x = x * 10
                    if num >= 2 ** count:
                            x = x + 1
                            num = num - 2 ** count
                    count = count - 1
            return x


def binaryWhole(num):
    """
      >>> binaryWhole(1001)
      9
      >>> binaryWhole(0)
      0
      >>> binaryWhole(10110)
      22
    """
    count = 0
    x = 0
    if num == 0:
            return 0
    else:
            count = len(str(num)) - 1
            while count >= 0:
                    if num >= 10 ** count:
                            x = x + 2 ** count
                            num = num - 10 ** count
                    count = count - 1
            return x


if __name__=='__main__':
    import doctest
    doctest.testmod()

