from gasp import games
from gasp import boards
from gasp import color

WIDTH = 4
HEIGHT = 4
MARGIN = 40
BOX_SIZE = 60
NUM_COLORS = {2 : color.LIGHTGRAY, 
        4 : color.WHEAT, 
        8 : color.CORAL, 
        16 : color.CORAL,


class Game(boards.SingleBoard):
    def __init__(self):
        self.init_singleboard((MARGIN, MARGIN), WIDTH, HEIGHT, BOX_SIZE)
        self.draw_all_outlines()

    def new_gamecell(self, i, j):
        return Tile(self, i, j)

class Tile(boards.GameCell):
    def __init__(self, board, i, j):
        self.init_gamecell(board, i, j)

class Box:
    def __init__(self, board, num):
        self.init_shape(self, board, num)

    def init_shape(self, board, num):
        self.color = NUM_COLORS[num]

game=Game()
game.mainloop()
