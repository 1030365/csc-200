def power(num, n):
    if n==0:
        return 1
    if n<0:
        return power(num, n+1)/num
    return power(num, n-1)*num

def nroot(num, root, digits=10):
    num2=num
    for i in range(num):
        if power(i+1,root)>num:
            for j in range(1,digits+1):
                num=nroot_rec(num, num2, root, j)
            return num

def nroot_rec(num, num2, root, digit):
    digit=power(10,-digit)
    for i in range(10):
        if power(num+(i+1)*digit,root)>num2:
            return num+i*digit
    return num
    
    

num=int(input('Number: '))
root=int(input('Root: '))

print(power(num,root))
print(nroot(num,root))
