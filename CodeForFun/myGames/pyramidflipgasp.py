from gasp import games
from gasp import color
from gasp import boards
from random import randint
import argparse
import time

parser=argparse.ArgumentParser()
parser.add_argument('width',default=4,type=int,nargs='?')
args=parser.parse_args()
BOX_SIZE = 40
MARGIN = 80
WIDTH = args.width
HEIGHT = WIDTH
TURNS = 2
COLORS = [color.BROWN, color.ORANGE, color.RED, color.GREEN]
LIVES = 3
TEXT_SIZE = 18

class Pyramid(boards.SingleBoard):
    def __init__(self):
        self.mode=0
        self.newturns = 0
        self.lives = LIVES
        self.init_singleboard((MARGIN, MARGIN), WIDTH, HEIGHT*2+1, BOX_SIZE)
        self.create_directions()
        self.draw_all_outlines()
        self.midline=[]
        for i in self.grid:
            self.midline.append(i[HEIGHT])
        for tile in self.midline:
            tile.set_color(color.BLACK)
        self.enable_cursor(0,HEIGHT+1)
        self.level = 1
        x, y = self.cell_to_coords(WIDTH//2,-1)
        self.status = games.Text(self, x, y, f'TURNS:{TURNS} LIVES:{LIVES} LEVEL:1', TEXT_SIZE, color.WHITE)
        self.new_level()

    def new_level(self):
        self.turns = TURNS+self.newturns
        self.make_goalboard()
        self.board=[]
        self.origin=[]
        for row in self.goalboard.copy():
            self.board.append(row.copy())
        self.answer=[]
        while len(self.answer)<self.turns:
            strg=(randint(0, WIDTH-1), randint(0, HEIGHT-1))
            if strg in self.answer:
                continue
            self.answer.append(strg)
        for tile in self.answer:
            self.click(tile[0], tile[1])
        for row in self.board.copy():
            self.origin.append(row.copy())

    def new_gamecell(self, i, j):
        return Square(self, i, j)

    def make_goalboard(self):
        self.goalboard=[]
        for i in range(0, WIDTH):
            self.goalboard.append([])
        for col in self.grid:
            y=-1
            for tile in col[:WIDTH]:
                y+=1
                if randint(0,1)==0:
                    tile.set_color(COLORS[0])
                    self.goalboard[y].append(0)
                else:
                    tile.set_color(COLORS[1])
                    self.goalboard[y].append(1)

    def update_board(self):
        self.mode=0
        for x in range(0, WIDTH):
            for y in range(0, HEIGHT):
                if self.board[y][x]==1:
                    self.grid[x][y+HEIGHT+1].set_color(COLORS[1])
                else:
                    self.grid[x][y+HEIGHT+1].set_color(COLORS[0])
        self.status.set_text(f'TURNS:{self.turns} LIVES:{self.lives} LEVEL:{self.level}')
        if self.turns == 0:
            self.check_board()

    def click(self, x, y):
        strg=[]
        for i in range(x-1, x+2):
            for j in range(y-1, y+2):
                strg.append((i,j))
        for tile in strg:
            self.flip(tile[0], tile[1])
        self.update_board()

    def keypress(self, key):
        if self.lives>0 and key == boards.K_RETURN:
            if self.mode==0:
                self.mode=1
                for x in range(0,WIDTH):
                    for y in range(0,HEIGHT):
                        if self.board[y][x]==self.goalboard[y][x]:
                            self.grid[x][y+HEIGHT+1].set_color(COLORS[3])
                        else:
                            self.grid[x][y+HEIGHT+1].set_color(COLORS[2])
            else:
                self.mode=0
                self.update_board()


    def mouse_up(self, xy, button):
        (x, y) = (xy[0], xy[1])
        (i, j) = self.coords_to_cell(x,y)
        if self.on_board(i, j) and j>HEIGHT and self.turns>0:
            self.move_cursor(i, j)
            self.turns-=1
            self.click(i, j-HEIGHT-1)

    def check_board(self):
        if self.board == self.goalboard:
            self.level += 1
            if randint(0,4)==0 and (TURNS+self.newturns)<(HEIGHT*WIDTH):
                self.newturns += 1
            if randint(0,2)==2:
                self.lives += 1
            self.new_level() 
        elif self.lives>1:
            self.lives-=1
            self.turns = TURNS+self.newturns
            self.board=[]
            for row in self.origin.copy():
                self.board.append(row.copy())
            self.update_board()
        else:
            self.status.set_text('GAME OVER')



    def flip(self, x, y):
        if x<WIDTH and y<HEIGHT and x>=0 and y>=0:
            if self.board[y][x]==1:
                self.board[y][x]=0
            else:
                self.board[y][x]=1



class Square(boards.GameCell):
    def __init__(self, board, i, j):
        self.init_gamecell(board, i, j)



game=Pyramid()
game.mainloop()
