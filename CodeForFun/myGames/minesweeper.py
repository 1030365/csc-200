from gasp import boards
from gasp import games
from gasp import color
from random import randint
import argparse

parser=argparse.ArgumentParser()
parser.add_argument('height', default=15, type=int, nargs='?')
args=parser.parse_args()

CHEATCODE = [1073741906, 1073741906, 1073741905, 1073741905, 1073741904, 1073741903, 1073741904, 1073741903, 98, 97, 13]
MARGIN = 60
TEXT_SIZE = 30
HEIGHT = args.height
WIDTH = HEIGHT+3
NUMMINES = (HEIGHT*WIDTH*4)//27
COLORS = [(0,210,0), color.LIME, (212,201,157), (255,242,191), color.RED]
BOX_SIZE = 600//HEIGHT
NUMCOLORS = [color.BLUE, color.LIME, color.RED, color.PURPLE, color.GOLD, color.BROWN, color.ORANGE, color.BLACK]

class Game(boards.SingleBoard):
    def __init__(self):
        self.cheatcode = [0,0,0,0,0,0,0,0,0,0,0]
        self.game_over = False
        self.cheater = False
        self.minespots = []
        while len(self.minespots) < NUMMINES:
            x, y = randint(0, WIDTH-1), randint(0, HEIGHT-1)
            if (x,y) in self.minespots:
                continue
            self.minespots.append((x,y))
        self.pattern = 0
        self.count = 0
        self.init_singleboard((MARGIN, MARGIN), WIDTH, HEIGHT, BOX_SIZE)
        self.draw_all_outlines()
        x, y = self.cell_to_coords(WIDTH//2,-1)
        y += BOX_SIZE/2
        self.win = False
        self.flags = NUMMINES
        self.status = games.Text(self, x, y, f'Flags: {NUMMINES}', TEXT_SIZE, color.WHITE)
        self.assign_nums()
        while True:
            i, j = randint(0, WIDTH-1), randint(0, HEIGHT-1)
            if not (i,j) in self.minespots:
                if self.grid[i][j].num == 0:
                    break
        self.grid[i][j].dig()

    def cheatmode(self):
        if self.cheater:
            return None
        x, y = self.cell_to_coords(WIDTH//2, HEIGHT)
        y += BOX_SIZE/1.5
        self.cheater = True
        self.cheat_status = games.Text(self, x, y, 'CHEAT MODE', TEXT_SIZE, color.WHITE)

    def checkwin(self):
        self.win = True
        for i in range(0, WIDTH):
            for j in range(0, HEIGHT):
                if self.grid[i][j].dug == False and self.grid[i][j].mine == False:
                    self.win = False
        if self.win:
            self.game_over = True
            self.status.set_text('YOU WIN!')

    def swap(self, nums, newnum):
        nums.append(newnum)
        return nums[1:]

    def keypress(self, key):
        if self.game_over:
            return None
        self.cheatcode = self.swap(self.cheatcode,key)
        if self.cheatcode == CHEATCODE:
            self.cheatmode()
        i, j = self.coords_to_cell(self.mouse_position()[0], self.mouse_position()[1])
        if key == 1073742049:
            if self.on_board(i, j):
                self.grid[i][j].flag()

        if key == games.K_SPACE:
            if self.on_board(i, j):
                if self.grid[i][j].dug:
                    self.grid[i][j].quickdig()
                else:
                    self.grid[i][j].dig()
        if key == 99 and self.cheater:
            self.flagcheat()
        if key == 104 and self.cheater:
            self.digcheat()
        if key == 101 and self.cheater:
            for i in range(0,100):
                self.flagcheat()
                self.digcheat()
        self.checkwin()

    def flagcheat(self):
        for i in range(0, WIDTH):
            for j in range(0, HEIGHT):
                if self.grid[i][j].num > 0:
                    count = 0
                    for ii in range(-1,2):
                        for jj in range(-1,2):
                            if not self.on_board(i+ii,j+jj):
                                continue
                            if not self.grid[i+ii][j+jj].dug:
                                count += 1
                    if count == self.grid[i][j].num:
                        for ii in range(-1,2):
                            for jj in range(-1,2):
                                if not self.on_board(i+ii,j+jj):
                                    continue
                                if not self.grid[i+ii][j+jj].flagged:
                                    self.grid[i+ii][j+jj].flag()

    def digcheat(self):
        for i in range(0, WIDTH):
            for j in range(0, HEIGHT):
                self.grid[i][j].quickdig()

    def assign_nums(self):
        for i in range(0, WIDTH):
            for j in range(0, HEIGHT):
                self.grid[i][j].findnum()

    def new_gamecell(self, i, j):
        self.pattern -= 1
        self.pattern *= -1
        self.count+=1
        if HEIGHT//2 == HEIGHT/2 and self.count == HEIGHT:
            self.count = 1
            self.pattern -= 1
            self.pattern *= -1
        if (i,j) in self.minespots:
            return Mine(self, i, j, self.pattern)
        return Grass(self, i, j, self.pattern)

    def mouse_up(self, xy, button):
        if self.game_over:
            return None
        x, y = xy[0], xy[1]
        i, j = self.coords_to_cell(x, y)
        if self.on_board(i, j):
            if button == 0 and self.is_pressed(1073742048) == 0:
                if self.grid[i][j].dug:
                    self.grid[i][j].quickdig()
                else:
                    self.grid[i][j].dig()
                self.checkwin()
            elif button == 2 or (button == 0 and self.is_pressed(1073742048) == 1):
                self.grid[i][j].flag()

class Square(boards.GameCell):
    def __init__(self, board, i, j, pattern):
        self.init_tile(board, i, j, pattern)

    def init_tile(self, board, i, j, pattern):
        self.color = COLORS[pattern]
        self.pattern = pattern
        self.flagged = False
        self.board = board
        self.x = i
        self.dug = False
        self.y = j
        self.mark = None
        self.init_gamecell(board, i, j)
        self.set_color(self.color)

    def flag(self):
        if self.dug:
            return None
        if self.flagged:
            self.flagged = False
            self.mark.destroy()
            self.board.flags += 1
        elif self.board.flags > 0:
            ii, jj = self.board.cell_to_coords(self.x, self.y)
            self.mark = games.Text(self.board, ii+BOX_SIZE/2, jj+BOX_SIZE/2, 'X', BOX_SIZE, color.RED)
            self.flagged = True
            self.board.flags -= 1
        self.board.status.set_text(f'Flags: {self.board.flags}')

class Mine(Square):
    def __init__(self, board, i, j, pattern):
        self.init_tile(board, i, j, pattern)
        self.mine = True
        self.num = 0

    def dig(self):
        if self.flagged:
            return None
        else:
            self.burst()

    def quickdig(self):
        pass

    def burst(self):
        self.set_color(COLORS[4])
        self.board.game_over = True
        self.board.status.set_text('GAME OVER')

    def findnum(self):
        pass

class Grass(Square):
    def __init__(self, board, i, j, pattern):
        self.init_tile(board, i, j, pattern)
        self.num = 0
        self.mine = False

    def dig(self):
        if self.flagged:
            return None
        if not self.dug:
            self.set_color(COLORS[self.pattern+2])
            self.dug = True
            i, j = self.board.cell_to_coords(self.x, self.y)
            if self.num > 0:
                self.proxmines = games.Text(self.board, i+BOX_SIZE/2, j+BOX_SIZE/2, str(self.num), BOX_SIZE, NUMCOLORS[self.num-1])
            else:
                for ii in range(self.x-1, self.x+2):
                    for jj in range(self.y-1, self.y+2):
                        if self.board.on_board(ii, jj):
                            self.board.grid[ii][jj].dig()
    def quickdig(self):
        if self.dug:
            if self.num == 0:
                return None
            counter = 0
            for i in range(self.x-1, self.x+2):
                for j in range(self.y-1, self.y+2):
                    if self.board.on_board(i, j):
                        if self.board.grid[i][j].flagged:
                            counter += 1
            if counter == self.num:
                for i in range(self.x-1, self.x+2):
                    for j in range(self.y-1, self.y+2):
                        if self.board.on_board(i, j):
                            self.board.grid[i][j].dig()

    def findnum(self):
        for i in range(self.x-1, self.x+2):
            if i == WIDTH or i == -1:
                continue
            for j in range(self.y-1, self.y+2):
                if j == HEIGHT or j == -1:
                    continue
                if self.board.grid[i][j].mine:
                    self.num += 1
game = Game()
game.mainloop()
