from gasp import games
from gasp import boards
from gasp import color

WIDTH = 20
HEIGHT = 20
MARGIN = 60
BOX_SIZE = 20
COUNTER_SIZE = BOX_SIZE/2 - 2
TEXT_SIZE = 25
BOARD_COLORS = [color.YELLOW, color.BROWN]
PLAYER1 = color.BLACK
PLAYER2 = color.WHITE
NEXT_PLAYER = {PLAYER1 : PLAYER2, PLAYER2 : PLAYER1}
COLOR = {PLAYER1 : "Black", PLAYER2 : "White"}

class Go(boards.SingleBoard):
    def __init__(self):
        self.init_singleboard((MARGIN, MARGIN), WIDTH, HEIGHT, BOX_SIZE)
        self.create_directions()
        self.resign = 0
        self.draw_all_outlines()
        self.passes = {PLAYER1 : 0, PLAYER2 : 0}
        self.game_over = 0
        self.counters=[]
        self.current_player = PLAYER1
        self.points = {PLAYER1 : 0, PLAYER2 : 0}
        self.scores = games.Text
        for x in range(0,WIDTH):
            for y in range(0,HEIGHT):
                if x==0 or x==(WIDTH-1) or y==0 or y==(HEIGHT-1):
                    self.grid[x][y].set_color(BOARD_COLORS[1])
                else:
                    self.grid[x][y].set_color(BOARD_COLORS[0])
        x, y = self.cell_to_coords(WIDTH//2, -1)
        self.status = games.Text(self, x, y, COLOR[PLAYER1] + '\'s turn', TEXT_SIZE, color.WHITE)
        x, y = self.cell_to_coords(WIDTH//2, HEIGHT+1.7)
        self.scores = games.Text(self, x, y, f'{COLOR[PLAYER1]}: {self.points[PLAYER1]}               {COLOR[PLAYER2]}: {self.points[PLAYER2]}', TEXT_SIZE, color.WHITE)
    def new_gamecell(self, i, j):
        return Square(self, i, j)

    def check_board(self, player):
        for col in self.grid[1:]:
            for spot in col[1:]:
                if spot.owner == NEXT_PLAYER[player] or spot.owner == None:
                    strg = [spot]
                    while True:
                        safe = False
                        for thing in strg:
                            if thing.side:
                                safe = True
                                break
                        if safe:
                            break
                        tests = []
                        for tile in strg:
                            tests.append(self.grid[tile.x-1][tile.y])
                            tests.append(self.grid[tile.x+1][tile.y])
                            tests.append(self.grid[tile.x][tile.y-1])
                            tests.append(self.grid[tile.x][tile.y+1])
                        for tile in tests:
                            if tile in strg:
                                continue
                            if tile.owner == player:
                                continue
                            strg.append(tile)
                            safe = True
                        if not safe:
                            for tile in strg:
                                tile.owner = player
                                if not tile.counter == None:
                                    tile.remove_counter()
                            break

    def check_game(self):
        for col in self.grid[1:]:
            for tile in col[1:]:
                if tile.owner == None:
                    return None
        self.game_over = 1
        if self.points[PLAYER1] > self.points[PLAYER2]:
            winner = PLAYER1
        else:
            winner = PLAYER2
        self.status.set_text(f'{COLOR[winner]} wins!')


    def update_scoreboard(self):
        self.points = {PLAYER1 : 0, PLAYER2 : 0}
        for col in self.grid[1:]:
            for tile in col[1:]:
                if not tile.owner == None:
                    self.points[tile.owner]+=1
        self.scores.set_text(f'{COLOR[PLAYER1]}: {self.points[PLAYER1]}               {COLOR[PLAYER2]}: {self.points[PLAYER2]}')

    def keypress(self, key):
        if key == games.K_RETURN and self.game_over == 0:
            self.resign = 0
            self.status.set_text(f'{COLOR[self.current_player]} passed, {COLOR[NEXT_PLAYER[self.current_player]]}\'s turn')
            if self.passes[self.current_player] == 0:
                self.passes[self.current_player] = 1
                self.current_player = NEXT_PLAYER[self.current_player]
            else:
                self.status.set_text(f'{COLOR[self.current_player]} passed twice, {COLOR[NEXT_PLAYER[self.current_player]]} wins!')
                self.game_over = 1
        if key == games.K_SPACE and self.game_over == 0:
            if self.resign == 2:
                self.game_over = 1
                self.status.set_text(f'{COLOR[self.current_player]} resigned, {COLOR[NEXT_PLAYER[self.current_player]]} wins!')
            else:
                self.resign +=1

    def mouse_up(self, xy, button):
        x, y = xy[0], xy[1]
        i, j = self.coords_to_cell(x, y)
        if i>0 and j>0 and self.on_board(i, j) and self.grid[i][j].counter == None and self.game_over == 0:
            if not self.grid[i][j].owner == NEXT_PLAYER[self.current_player]:
                self.resign = 0
                self.grid[i][j].add_counter(self.current_player)
                self.passes[self.current_player] = 0
                self.check_board(self.current_player)
                self.update_scoreboard()
                self.current_player = NEXT_PLAYER[self.current_player]
                self.status.set_text(COLOR[self.current_player] + '\'s turn')
                self.check_game()
            

class Square(boards.GameCell):
    def __init__(self, board, i, j):
        self.board = board
        self.counter = None
        self.owner = None
        self.x = i
        self.y = j
        self.init_gamecell(board, i+0.5, j+0.5)
        if self.x == 1 or self.x == WIDTH-1 or self.y == 1 or self.y == HEIGHT-1:
            self.side = True
        else:
            self.side = False

    def add_counter(self, player):
        self.counter = Counter(self.board, self.screen_x, self.screen_y, player)
        self.owner = self.board.current_player

    def remove_counter(self):
        self.counter.destroy()
        self.counter = None


class Counter(games.Circle):
    def __init__(self, board, x, y, player):
        self.init_circle(board, x, y+1/10000, COUNTER_SIZE, player, filled=True)

game = Go()
game.mainloop()
