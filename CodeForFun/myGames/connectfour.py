from gasp import games
from gasp import color
from gasp import boards
from random import randint


BOX_SIZE = 80
MARGIN = 0
WIDTH = 7
HEIGHT = 6
BOARD_COLORS = [color.BLUE, color.WHITE, color.BLACK]
PLAYER1 = color.RED
PLAYER2 = color.YELLOW
NEXT_PLAYER = {PLAYER1 : PLAYER2, PLAYER2 : PLAYER1}
COUNTER_SIZE = BOX_SIZE*0.4
DIRECTIONS = [boards.UP, boards.RIGHT, boards.UP_RIGHT, boards.UP_LEFT]
ALPHA = 0.3
HIGHLIGHT = {PLAYER1 : (), PLAYER2 : ()}
HIGHLIGHT[PLAYER1] = ((1-ALPHA) * BOARD_COLORS[1][0] + ALPHA * PLAYER1[0],
        (1-ALPHA) * BOARD_COLORS[1][1] + ALPHA * PLAYER1[1],
        (1-ALPHA) * BOARD_COLORS[1][2] + ALPHA * PLAYER1[2])
HIGHLIGHT[PLAYER2] = ((1-ALPHA) * BOARD_COLORS[1][0] + ALPHA * PLAYER2[0],
        (1-ALPHA) * BOARD_COLORS[1][1] + ALPHA * PLAYER2[1],
        (1-ALPHA) * BOARD_COLORS[1][2] + ALPHA * PLAYER2[2])

class Connectfour(boards.SingleBoard):
    def __init__(self):
        self.loading = True
        self.init_singleboard((MARGIN, MARGIN), WIDTH, HEIGHT, BOX_SIZE)
        self.create_directions()
        self.draw_all_outlines()
        self.game_over = 0
        self.current_player = PLAYER1
        self.loading = False

    def new_gamecell(self, i, j):
        return Square(self, i, j)

    def mouse_up(self, xy, button):
        if button > 2:
            return None
        x, y = xy[0], xy[1]
        i, j = self.coords_to_cell(x, y)
        if self.on_board(i,j) and self.game_over == 0:
            if self.grid[i][0].owner == None:
                self.drop(i)
                self.update_game()
                self.current_player = NEXT_PLAYER[self.current_player]

    def update_game(self):
        count = 0
        for i in range(0, WIDTH):
            for j in range(0, HEIGHT):
                if self.grid[i][j].checkwin() != None:
                    self.game_over = 1
                if self.grid[i][j].owner != None:
                    count += 1
        if count == HEIGHT * WIDTH:
            self.game_over = 1

    def shadowdrop(self, i):
        j = 0
        while True:
            if j == HEIGHT - 1:
                return j
            elif self.grid[i][j+1].owner == None:
                j += 1
            else:
                return j

    def tick(self):
        if self.loading or self.game_over == 1:
            return None
        x, y = self.coords_to_cell(self.mouse_position()[0], self.mouse_position()[1])
        if self.on_board(x, y):
            for i in range(0, WIDTH):
                if i == x:
                    continue
                if self.grid[i][0].owner == None:
                    self.grid[i][self.shadowdrop(i)].counter.set_color(BOARD_COLORS[1])
            if self.grid[x][0].owner == None:
                self.grid[x][self.shadowdrop(x)].counter.set_color(HIGHLIGHT[self.current_player])

    def drop(self, i):
        j = 0
        while True:    
            if j == HEIGHT-1:
                self.grid[i][j].add_counter(self.current_player)
                break
            elif self.grid[i][j+1].owner == None:
                j+=1
                continue
            self.grid[i][j].add_counter(self.current_player)
            break

class Square(boards.GameCell):
    def __init__(self, board, i, j):
        self.board = board
        self.owner = None
        self.init_gamecell(board, i, j)
        self.set_color(BOARD_COLORS[0])
        self.counter = Counter(self.board, self.screen_x, self.screen_y, BOARD_COLORS[1])

    def add_counter(self, player):
        self.counter.set_color(player)
        self.owner = player
    
    def checkwin(self):
        if self.owner == None:
            return None
        for d in DIRECTIONS:
            strg = []
            s = self
            for i in range(0,3):
                if s.direction[d] != None:
                    s = s.direction[d]
                    strg.append(s.owner)
            if strg == [self.owner]*3:
                return self.owner



class Counter(games.Circle):
    def __init__(self, board, x, y, player):
        boarder = Boarder(board, x, y, BOARD_COLORS[2])
        self.init_circle(board, x+BOX_SIZE/2, y+BOX_SIZE/2, COUNTER_SIZE, player, filled=True)



class Boarder(games.Circle):
    def __init__(self, board, x, y, shade):
        self.init_circle(board, x+BOX_SIZE/2, y+BOX_SIZE/2, COUNTER_SIZE+1, shade)


game = Connectfour()
game.mainloop()
