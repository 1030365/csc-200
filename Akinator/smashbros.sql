CREATE TABLE "Favmoves" ("move" TEXT, "fighter" TEXT) ;

INSERT INTO Favmoves (move, fighter) VALUES ('Jab','Steve') ;
INSERT INTO Favmoves (move, fighter) VALUES ('ForwardTilt','Simon') ;
INSERT INTO Favmoves (move, fighter) VALUES ('DownTilt','Byleth') ;
INSERT INTO Favmoves (move, fighter) VALUES ('UpTilt','Pichu') ;
INSERT INTO Favmoves (move, fighter) VALUES ('ForwardSmash','MinMin') ;
INSERT INTO Favmoves (move, fighter) VALUES ('UpSmash','Sephiroth') ;
INSERT INTO Favmoves (move, fighter) VALUES ('DownSmash','KingKRool') ;
INSERT INTO Favmoves (move, fighter) VALUES ('NeutralAir','Mewtwo') ;
INSERT INTO Favmoves (move, fighter) VALUES ('ForwardAir','Roy') ;
INSERT INTO Favmoves (move, fighter) VALUES ('UpAir','Mario') ;
INSERT INTO Favmoves (move, fighter) VALUES ('DownAir','CaptainFalcon') ;
INSERT INTO Favmoves (move, fighter) VALUES ('BackAir','MetaKnight') ;
INSERT INTO Favmoves (move, fighter) VALUES ('NeutralB','Shulk') ;
INSERT INTO Favmoves (move, fighter) VALUES ('SideB','Sephiroth') ;
INSERT INTO Favmoves (move, fighter) VALUES ('UpB','Sephiroth') ;
INSERT INTO Favmoves (move, fighter) VALUES ('DownB','Hero') ;
INSERT INTO Favmoves (move, fighter) VALUES ('ForwardThrow','Mewtwo') ;
INSERT INTO Favmoves (move, fighter) VALUES ('UpThrow','Sephiroth') ;
INSERT INTO Favmoves (move, fighter) VALUES ('BackThrow','Lucas') ;
INSERT INTO Favmoves (move, fighter) VALUES ('DownThrow','CaptainFalcon') ;
INSERT INTO Favmoves (move, fighter) VALUES ('FinalSmash','Sephiroth') ;

CREATE TABLE "Favchars" ("fighter" TEXT, "tier" INTEGER) ;

INSERT INTO Favchars (fighter, tier) VALUES ('Steve','1') ;
INSERT INTO Favchars (fighter, tier) VALUES ('Sephiroth','2') ;
INSERT INTO Favchars (fighter, tier) VALUES ('Terry','3') ;
INSERT INTO Favchars (fighter, tier) VALUES ('Mythra','4') ;
INSERT INTO Favchars (fighter, tier) VALUES ('Sonic','5') ;

SELECT Favchars.fighter,Favchars.tier,(SELECT COUNT(*) FROM Favmoves WHERE fighter=Favchars.fighter) AS "# of Favmoves" FROM Favchars ;
