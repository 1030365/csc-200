import sqlite3

conn=sqlite3.connect('Bigmoves.sqlite')
cur=conn.cursor()

cur.execute('DROP TABLE IF EXISTS Moves')

cur.execute('CREATE TABLE Moves (char TEXT, count INTEGER)')

fname=input('File Name? ')
if fname=='': fname='bigmoves.txt'
fh=open(fname)
for line in fh:
    if not line.startswith('Name: '): continue
    char=line[6:]
    cur.execute('SELECT count FROM Moves WHERE char = ? ',(char,))
    row=cur.fetchone()
    if row is None:
        cur.execute('INSERT INTO Moves (char,count) VALUES (?,1)',(char,))
    else:
        cur.execute('UPDATE Moves SET count = count + 1 WHERE char = ?',(char,))
    conn.commit()

sqlstr='SELECT char, count FROM Moves ORDER BY count DESC'

for row in cur.execute(sqlstr):
    print(f'{row[0]} has {row[1]} big move(s)')
