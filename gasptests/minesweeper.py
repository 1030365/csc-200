from gasp import games
from gasp import color
from gasp import boards
from random import randint

BOX_SIZE = 40
MARGIN = 60

MINE_SIZE = BOX_SIZE/2 - 3

NUMBER_SIZE = 24
TEXT_SIZE = 18

FLAG = ((BOX_SIZE/4, 2), (BOX_SIZE/2, BOX_SIZE-2), (3*BOX_SIZE/4, 2))

class GridBox(boards.GameCell):
    def __init__(self, screen, i, j):
        self.init_gamecell(screen, i, j)
        self.flagged = 0
        self.shown = 0
        self.image = None
        self.flag = None

class Mine(boards.Container, GridBox):
    def __init__(self, board, i, j):
        self.init_gridbox(board, i, j)
        self.init_container(['image', 'flag'])

    def reveal_mine(self):
        self.set_color(color.red)
        self.image = games.Circle(self.screen,
                self.xpos() + BOX_SIZE/2,
                self.ypos() + BOX_SIZE/2,
                MINE_SIZE,
                color.black,
                filled=True)
        if self.flagged:
            self.flag.raise_object()

class EmptyBox(boards.Container, GridBox):
    def __init__(self, board, i, j):
        self.init_gridbox(board, i, j)
        self.init_container(['image', 'flag'])

    def reveal_mine(self):
        pass

class Minesweeper(boards.SingleBoard):
    def __init__(self, n_cols, n_rows, mines_to_find):
        self.mines_to_find = mines_to_find
        self.flag_count = 0
        self.unknown_count = n_cols * n_rows
        self.game_over = 0

        self.init_singleboard((MARGIN, MARGIN), n_cols, n_rows, BOX_SIZE)
