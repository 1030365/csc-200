from gasp import *

grid_size = 30
margin = grid_size
background_color = color.BLACK
wall_color = color.BLUE
the_layout = [
  "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%",
  "%.....%.................%.....%",
  "%o%%%.%.%%%.%%%%%%%.%%%.%.%%%o%",
  "%.%.....%......%......%.....%.%",
  "%...%%%.%.%%%%.%.%%%%.%.%%%...%",
  "%%%.%...%.%.........%.%...%.%%%",
  "%...%.%%%.%.%%% %%%.%.%%%.%...%",
  "%.%%%.......%GG GG%.......%%%.%",
  "%...%.%%%.%.%%%%%%%.%.%%%.%...%",
  "%%%.%...%.%.........%.%...%.%%%",
  "%...%%%.%.%%%%.%.%%%%.%.%%%...%",
  "%.%.....%......%......%.....%.%",
  "%o%%%.%.%%%.%%%%%%%.%%%.%.%%%o%",
  "%.....%........P........%.....%",
  "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"]

class Immovable:
    def is_a_wall(self):
        return 0

class Nothing(Immovable):
    pass

class Maze:
    def __init__(self):
        self.have_window = 0
        self.game_over = 0
        self.set_layout(the_layout)
        
    def set_layout(self, layout):
        height = len(layout)
        width = len(layout[0])
        self.make_window(width, height)
        self.make_map(width, height)

        max_y = height - 1
        for x in range(width):
            for y in range(height):
                char = layout[max_y - y][x]
                self.make_object((x, y), char)

    def make_window(self, width, height):
        grid_width = (width-1) * grid_size
        grid_height = (height-1) * grid_size
        screen_width = 2*margin + grid_width
        screen_height = 2*margin + grid_height
        begin_graphics(screen_width,
                screen_height,
                background_color)

    def to_screen(self, point):
        (x, y) = point
        x = x*grid_size + margin
        y = y*grid_size + margin
        return (x, y)

    def make_map(self, width, height):
        self.width = width
        self.height = height
        self.map = []
        for y in range(height):
            new_row = []
            for x in range(width):
                new_row.append(Nothing())
            self.map.append(new_row)

    def make_object(self, point, character):
        (x, y) = point
        if character == '%':
            self.map[y][x] = Wall(self, point)

    def finished(self):
        return self.game_over

    def play(self):
        sleep(0.05)

    def done(self):
        end_graphics()
        self.map = []

    def object_at(self, point):
        (x,y) = point

        if y < 0 or y >= self.height:
            return Nothing()

        if x < 0 or x >= self.width:
            return Nothing()

        return self.map[y][x]

class Wall(Immovable):
    def __init__(self, maze, point):
        self.place = point
        self.screen_point = maze.to_screen(point)
        self.maze = maze
        self.draw()

    def draw(self):
        (screen_x, screen_y) = self.screen_point
        dot_size = grid_size * 0.2
        Circle((screen_x, screen_y), dot_size,
                color=wall_color, filled=True)
        (x, y) = self.place
        neighbors = [(x+1, y), (x-1, y),
                (x, y+1), (x, y-1)]
        for neighbor in neighbors:
            self.check_neighbor(neighbor)

    def check_neighbor(self, neighbor):
        maze = self.maze
        object = maze.object_at(neighbor)
        if object.is_a_wall():
            here = self.screen_point
            there = maze.to_screen(neighbor)
            Line(here, there, color = wall_color)

    def is_a_wall(self):
        return 1

class Movable:
    def __init__(self, maze, point, speed):
        self.maze = maze
        self.place = point
        self.speed = speed

class Pacman(Movable):
    def __init__(self, maze, point):
        Movable.__init__(self, maze, point,
                pacman_speed)

    def move(self):
        keys = keys_pressed()
        if 'z' in keys: self.move_left()
        if 'x' in keys: self.move_right()
        if ';' in keys: self.move_up()
        if '.' in keys: self.move_down()

    def move_left(self):
        self.try_move((-1, 0))

    def move_right(self):
        self.try_move((1, 0))

    def move_up(self):
        self.try_move((0, 1))

    def move_down(self):
        self.try_move((0, -1))

    def try_move(self, move):
        (move_x, move_y) = move
        (current_x, current_y) = self.place
        (nearest_x, nearest_y) = (
                self.nearest_grid_point())

        if self.furthest_move(move) == (0, 0):
            return

        if move_x != 0 and current_y != nearest_y:
            move_x = 0
            move_y = nearest_y - current_y

        elif move_y != 0 and current_x != nearest_x:
            move_y = 0
            move_x = nearest_x - current_x

        move = self.furthest_move((move_x, move_y))
        self.move_by(move)

the_maze = Maze()
while not the_maze.finished():
    the_maze.play()
the_maze.done()
