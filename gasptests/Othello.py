from gasp import games
from gasp import boards
from gasp import color

# creates constant variables in the program; the namespace variables
BOX_SIZE = 40
MARGIN = 60
COUNTER_SIZE = BOX_SIZE/2 - 3
TEXT_SIZE = 18
PLAYER1 = color.WHITE
PLAYER2 = color.BLACK
NEXT_PLAYER = {PLAYER1 : PLAYER2,
        PLAYER2 : PLAYER1}
COLOR = {PLAYER1 : "White",
        PLAYER2 : "Black"}
ALL_DIRECTIONS = [boards.LEFT, boards.UP_LEFT, boards.UP, boards.UP_RIGHT,
        boards.RIGHT, boards.DOWN_RIGHT, boards.DOWN, boards.DOWN_LEFT]


def check_any_captures(square):
    # checks if a peice can be placed on a given square
    global capture_found, player_to_check
    if not capture_found and square.counter == None:
        capture_found = square.can_capture(player_to_check)

class Square(boards.GameCell):
    def __init__(self, board, i, j):
        # set starting variables
        self.init_gamecell(board, i, j)
        self.counter = None

    def force_add_counter(self, player):
        # place a counter on the board with no conditions
        self.counter = Counter(self.board, self.screen_x + BOX_SIZE/2,
                self.screen_y + BOX_SIZE/2, player)
        self.board.num_counters[player] = self.board.num_counters[player] + 1

    def can_capture(self, player):
        other = NEXT_PLAYER[player]
        
        # checking for opposite player's peices in every direction
        for d in ALL_DIRECTIONS:
            if self.direction[d] != None and self.direction[d].counter != None:
                if self.direction[d].counter.is_player(other):
                    s = self.direction[d].direction[d]
        
                    # repeat until either the next peice is the player's color or doesn't exist.
                    while s != None and s.counter != None and s.counter.is_player(other):
                        s = s.direction[d]
                    if s != None and s.counter != None:
                        
                        # return true if a viable capture is found, false if otherwise
                        return 1
        return 0

    def capture(self, player):
        # flips over opponents peices between a placed counter and an existing counter
        
        other = NEXT_PLAYER[player]
        
        # finds peices that can be flipped
        for d in ALL_DIRECTIONS:
            if self.direction[d] != None and self.direction[d].counter != None:
                if self.direction[d].counter.is_player(other):
                    s = self.direction[d].direction[d]
                    while s != None and s.counter != None and s.counter.is_player(other):
                        s = s.direction[d]
                    
                    # flips all flippable peices
                    if s != None and s.counter != None: 
                        m = self.direction[d]
                        while m != s:
                            m.counter.flip()
                            m = m.direction[d]

    def add_counter(self, player):
        # Adds a counter if it can capture peices
        if self.counter == None and self.can_capture(player):
            self.force_add_counter(player)
            self.capture(player)
            
            # returns true if succesful, false otherwise
            return 1
        return 0


class Othello(boards.SingleBoard):
    def __init__(self):
        # creates the board
        self.init_singleboard((MARGIN, MARGIN), 8, 8, BOX_SIZE)
        
        self.create_directions()
        
        # creates cursor
        self.enable_cursor(3,3)
        
        # sets other starting variables
        self.current_player = PLAYER1
        self.game_over = 0
        self.num_counters = {PLAYER1 : 0,
                PLAYER2 : 0}
        
        # sets starting peeices
        self.grid[3][3].force_add_counter(PLAYER1)
        self.grid[4][4].force_add_counter(PLAYER1)
        self.grid[3][4].force_add_counter(PLAYER2)
        self.grid[4][3].force_add_counter(PLAYER2)
        
        # creates text
        x, y = self.cell_to_coords(4, -1)
        self.status = games.Text(self, x, y, COLOR[PLAYER1] + "'s turn",
                TEXT_SIZE, color.WHITE)
        self.draw_all_outlines()

    def new_gamecell(self, i, j):
        # replaces gamecells with Square class
        return Square(self, i, j)

    def mouse_up(self, xy, button):
        # enables mouse
        
        # stop functioning if game is over
        if self.game_over:
            return
        
        # moves cursor to mouse
        x, y = xy[0], xy[1]
        i, j = self.coords_to_cell(x, y)
        if self.on_board(i, j):
            self.move_cursor(i, j)
            self.do_counter()
    
    def keypress(self, key):
        # allows keys to be registered
        self.handle_cursor(key)
        if self.game_over:
            return
        
        # if key is enter, attempt to place a counter
        if key == games.K_RETURN:
            self.do_counter()
            
    def do_counter(self):
        # attempt to place a counter where the cursor is
        
        global capture_found, player_to_check
        
        # if a counter is placed
        if self.cursor.add_counter(self.current_player):
            
            # end the game if only one player has peices on the board
            if self.num_counters[NEXT_PLAYER[self.current_player]] == 0:
                self.end_game()
                return
            
            # end the game if the board is full
            if self.num_counters[PLAYER1] + self.num_counters[PLAYER2] == 64:
                self.end_game()
                return
            
            # end game if nobody has a legal move (bugged)
            capture_found = 0
            player_to_check = self.current_player
            self.map_grid(check_any_captures)
            if capture_found:
                self.status.set_text("No move for " +
                        COLOR[NEXT_PLAYER[self.current_player]] +
                        ": " + COLOR[self.current_player] +
                        "'s turn")

            # skip the next player's turn if they don't have a legal move
            capture_found = 0
            player_to_check = NEXT_PLAYER[self.current_player]
            self.map_grid(check_any_captures)
            if capture_found:
                self.current_player = NEXT_PLAYER[self.current_player]
                self.status.set_text(COLOR[self.current_player] + "'s turn")    
            else:
                self.status.set_text("No move for " +
                        COLOR[NEXT_PLAYER[self.current_player]] +
                        ": " + COLOR[self.current_player] +
                        "'s turn")

    def end_game(self):
        # ends the game and declares the winner
        
        # notify program game is over
        self.game_over = 1
        
        # remove cursor from screen
        self.disable_cursor()
        
        # declare winner
        if self.num_counters[PLAYER1] > self.num_counters[PLAYER2]:
            self.status.set_text("Game over: " + COLOR[PLAYER1] + " wins!")
        elif self.num_counters[PLAYER1] < self.num_counters[PLAYER2]:
            self.status.set_text("Game over: " + COLOR[PLAYER2] + " wins!")
        else:
            self.status.set_text("Game over: Draw!")
        
class Counter(games.Circle):
    def __init__(self, board, x, y, player):
        # sets its shape to a circle
        self.init_circle(board, x, y, COUNTER_SIZE, player, filled=True)

    def is_player(self, player):
        # checks if the counter belongs to a certain player
        return player == self.get_color()

    def flip(self):
        # flips counter over and converts it to the opposite player's color
        
        # changes color of counter
        old = self.get_color()
        new = NEXT_PLAYER[old]
        self.set_color(new)
        
        # updates player scores
        self.screen.num_counters[old] = self.screen.num_counters[old] - 1
        self.screen.num_counters[new] = self.screen.num_counters[new] + 1

# creates and runs Othello
othello = Othello()
othello.mainloop()
