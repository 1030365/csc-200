from gasp.utils import read_yesorno
from random import randint


best=-1
while True:
    num=randint(1,1000)
    count=0
    guess=0
    print('OK, I\'ve thought of a number between 1 and 1000.\n')
    while not guess==num:
        guess=int(input('Make a guess: '))
        if guess<num:
            print('That\'s too low.\n')
        elif guess>num:
            print('That\'s too high.\n')
        count+=1
    print(f'That was my number. Well done!\n\nYou took {count} guesses.')
#    if best==-1 or best>count:
#        best=count
#        print(f'New High Score! {count}!')
    if (input('Would you like to play another game? ')).upper()=='NO':
        break
#print(f'Your best score was {best}')
print('OK. Bye!')


'''#I guess if you are in a loop that is in a loop, the break command will take you to the loop the initial loop is in.
for i in range(1,5):
    print(f'Loop 1, trial {i}.0')
    for w in range(1,5):
        if w==4:
            break
        else:
            print(f'Loop 2, trial {i}.{w}')
    print('Loop 2 broken from or finished')
    if i==3:
        break
    else:
        print(f'I should break after trial 3.3, but am now going into {i}.0')
print('Loop 1 has been broken or finished')
#My guess was correct
'''



