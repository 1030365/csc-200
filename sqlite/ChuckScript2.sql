CREATE TABLE User (
    id     INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    name   TEXT UNIQUE,
    email  TEXT
) ;

CREATE TABLE Course (
    id     INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    title  TEXT UNIQUE
) ;

CREATE TABLE Member (
    user_id     INTEGER,
    course_id   INTEGER,
	role        INTEGER,
    PRIMARY KEY (user_id, course_id)
) ;

INSERT INTO User (name, email) VALUES ('Liam', '1030365@apsva.us');
INSERT INTO User (name, email) VALUES ('Obama', 'Obama@Llama');
INSERT INTO User (name, email) VALUES ('Squidward', 'Sward@nyeh.org');

INSERT INTO Course (title) VALUES ('Python');
INSERT INTO Course (title) VALUES ('SQL');
INSERT INTO Course (title) VALUES ('PHP');

INSERT INTO Member (user_id, course_id, role) VALUES (2, 1, 1);
INSERT INTO Member (user_id, course_id, role) VALUES (3, 1, 0);
INSERT INTO Member (user_id, course_id, role) VALUES (1, 1, 0);

INSERT INTO Member (user_id, course_id, role) VALUES (2, 2, 0);
INSERT INTO Member (user_id, course_id, role) VALUES (1, 2, 1);

INSERT INTO Member (user_id, course_id, role) VALUES (3, 3, 1);
INSERT INTO Member (user_id, course_id, role) VALUES (2, 3, 0);

SELECT User.name, Member.role, Course.title
  FROM User JOIN Member JOIN Course
  ON Member.user_id = User.id AND Member.course_id = Course.id
  ORDER BY Course.title, Member.role DESC, User.name 
