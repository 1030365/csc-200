CREATE TABLE "Artist" (
    "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE, 
    "name" TEXT) ;

CREATE TABLE "Album" (
    "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE, 
    artist_id INTEGER,
    "title" TEXT) ;

CREATE TABLE "Genre" (
    "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE, 
    "name" TEXT) ;

CREATE TABLE "Track" (
    "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL UNIQUE, 
    album_id INTEGER, genre_id INTEGER, len INTEGER, rating INTEGER, 
    "title" TEXT, "count" INTEGER) ;


INSERT INTO Artist (name) VALUES ('Led Zepplin') ;
INSERT INTO Artist (name) VALUES ('AC/DC') ;
INSERT INTO Artist (name) VALUES ('Imagine Dragons') ;
INSERT INTO Artist (name) VALUES ('Toby Fox') ;

INSERT INTO Genre (name) VALUES ('Rock') ;
INSERT INTO Genre (name) VALUES ('Metal') ;

INSERT INTO Album (title, artist_id) VALUES ('Who Made Who', 2) ;
INSERT INTO Album (title, artist_id) VALUES ('IV', 1) ;
INSERT INTO Album (title, artist_id) VALUES ('Evolve', 3) ;
INSERT INTO Album (title, artist_id) VALUES ('Undertale', 4) ;

INSERT INTO Track (title, rating, len, count, album_id, genre_id)
    VALUES ('Black Dog', 5, 297, 0, 2, 1) ;
INSERT INTO Track (title, rating, len, count, album_id, genre_id)
    VALUES ('Stairway', 5, 482, 0, 2, 1) ;
INSERT INTO Track (title, rating, len, count, album_id, genre_id)
    VALUES ('About to Rock', 5, 313, 0, 1, 2) ;
INSERT INTO Track (title, rating, len, count, album_id, genre_id)
    VALUES ('Who Made Who', 5, 207, 0, 1, 2) ;
INSERT INTO Track (title, rating, len, count, album_id, genre_id)
    VALUES ('Believer', 5, 205, 0, 3, 1) ;
INSERT INTO Track (title, rating, len, count, album_id, genre_id)
    VALUES ('Megalovania', 5, 156, 0, 4, 1) ;
INSERT INTO Track (title, rating, len, count, album_id, genre_id)
    VALUES ('Metal Crusher', 4, 63, 0, 4, 2) ;
INSERT INTO Track (title, rating, len, count, album_id, genre_id)
    VALUES ('Thunder', 5, 187, 0, 3, 1) ;

SELECT Track.title, Artist.name, Album.title, Genre.name, Track.len 
FROM Track JOIN Genre JOIN Album JOIN Artist 
    ON Track.genre_id = Genre.id AND Track.album_id = Album.id 
    AND Album.artist_id = Artist.id ;
