from gasp import *
from random import randint
from time import sleep


class RobotsGame:
    LEFT_KEY = "a"
    UP_LEFT_KEY = "q"
    UP_KEY = "w"
    UP_RIGHT_KEY = "e"
    RIGHT_KEY = "d"
    DOWN_RIGHT_KEY = "c"
    DOWN_KEY = "s"
    DOWN_LEFT_KEY = "z"
    TELEPORT_KEY = "x"
    RIGHT_EDGE = 63
    LEFT_EDGE = 0
    TOP_EDGE = 47
    BOTTOM_EDGE = 0
    junk=[]
    NUMBOTS=1000
    robots=[]

    def __init__(self):
        begin_graphics()
        self.finished = False
        for i in range(0,self.NUMBOTS):
            self.robots.append(Robot())
        self.player = Player()

    def next_move(self):
        self.player.move()
        for robot in self.robots:
            robot.move(self.player)
            if self.player.check_collisions(robot):
                self.finished=True
        self.check_collisions()

    def over(self):
        end_graphics()

    def robot_crashed(self,the_bot):
        for a_bot in self.robots:
            if a_bot==the_bot:
                return False
            if (a_bot.x,a_bot.y)==(the_bot.x,the_bot.y):
                return a_bot
        return False

    def collided(self,thing1,list_of_things):
        for thing2 in list_of_things:
            if (thing1.x,thing1.y)==(thing2.x,thing2.y):
                return True
        return False

    def check_collisions(self):
        self.surviving_robots=[]
        for bot in self.robots:
            if self.collided(bot,self.junk):
                continue
            self.jbot=self.robot_crashed(bot)
            if self.jbot==False:
                self.surviving_robots.append(bot)
            else:
                remove_from_screen(self.jbot.shape)
                self.jbot.shape = Box((10*self.jbot.x, 10*self.jbot.y), 10, 10, filled=True)
                self.junk.append(self.jbot)
        self.robots=[]
        for bot in self.surviving_robots:
            if not self.collided(bot,self.junk):
                self.robots.append(bot)
        if self.robots==[]:
            print('Ya did it!')
            time.sleep(3)
            self.finished=True


class Player:
    def __init__(self):
        self.place()

    def place(self):
        while True:
            self.redo=0
            self.x = randint(RobotsGame.LEFT_EDGE, RobotsGame.RIGHT_EDGE)
            self.y = randint(RobotsGame.BOTTOM_EDGE, RobotsGame.TOP_EDGE)
            for bot in RobotsGame.robots:
                if (self.x,self.y)==(bot.x,bot.y):
                    self.redo=1
            if self.redo==1:
                continue
            break

        self.shape = Circle((10 * self.x+5, 10 * self.y+5), 5, filled=True)

    def teleport(self):
        remove_from_screen(self.shape)
        self.place()

    def move(self):
        key = update_when("key_pressed")

        while key == RobotsGame.TELEPORT_KEY:
            self.teleport()
            key = update_when("key_pressed")

        if key == RobotsGame.RIGHT_KEY and self.x < RobotsGame.RIGHT_EDGE:
            self.x += 1
        elif key == RobotsGame.DOWN_RIGHT_KEY:
            if self.x < RobotsGame.RIGHT_EDGE:
                self.x += 1
            if self.y > RobotsGame.BOTTOM_EDGE:
                self.y -= 1
        elif key == RobotsGame.DOWN_KEY and self.y > RobotsGame.BOTTOM_EDGE:
            self.y -= 1
        elif key == RobotsGame.DOWN_LEFT_KEY:
            if self.x > RobotsGame.LEFT_EDGE:
                self.x -= 1
            if self.y > RobotsGame.BOTTOM_EDGE:
                self.y -= 1
        elif key == RobotsGame.LEFT_KEY and self.x > RobotsGame.LEFT_EDGE:
            self.x -= 1
        elif key == RobotsGame.UP_LEFT_KEY:
            if self.x > RobotsGame.LEFT_EDGE:
                self.x -= 1
            if self.y < RobotsGame.TOP_EDGE:
                self.y += 1
        elif key == RobotsGame.UP_KEY and self.y < RobotsGame.TOP_EDGE:
            self.y += 1
        elif key == RobotsGame.UP_RIGHT_KEY:
            if self.x < RobotsGame.RIGHT_EDGE:
                self.x += 1
            if self.y < RobotsGame.TOP_EDGE:
                self.y += 1

        move_to(self.shape, (10 * self.x, 10 * self.y))

    def check_collisions(self,target):
        if (self.x,self.y)==(target.x,target.y):
            return True
        else:
            return False

class Robot:
    def __init__(self):
        self.place()

    def place(self):
        self.x = randint(RobotsGame.LEFT_EDGE, RobotsGame.RIGHT_EDGE)
        self.y = randint(RobotsGame.BOTTOM_EDGE, RobotsGame.TOP_EDGE)
        self.shape = Box((10 * self.x, 10 * self.y), 10, 10)

    def move(self, player):
        if self.x < player.x:
            self.x += 1
        elif self.x > player.x:
            self.x -= 1

        if self.y < player.y:
            self.y += 1
        elif self.y > player.y:
            self.y -= 1

        move_to(self.shape, (10 * self.x, 10 * self.y))
    

game = RobotsGame()

while not game.finished:
    game.next_move()

game.over()
