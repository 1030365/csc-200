from gasp import *
from random import randint

def place_player():
    global player
    player=Player()
    (player.x,player.y)=(randint(0,63),randint(0,47))

def safely_place_player():
    global player
    place_player()
    while collided(player,robots):
        place_player()
    player.shape=Circle((10*player.x+5,10*player.y-5), 5, filled=True)

def collided(thing1,list_of_things):
    global player,robots
    for thing2 in list_of_things:
        if thing1.x==thing2.x and thing1.y==thing2.y:
            return True
    return False

def place_robots():
    global robots,numbots
    robots=[]
    while len(robots)<numbots:
        robot=Robot()
        (robot.x,robot.y)=(randint(0,63),randint(0,47))
        if not collided(robot,robots):
            robot.shape=Box((10*robot.x+5,10*robot.y-5), 10, 10)
            robots.append(robot)
        


def move_player():
    global player
    print("I'm moving...")
    key=update_when('key_pressed')
    while key=='9':
        remove_from_screen(player.shape)
        safely_place_player()
        key=update_when('key_pressed')
    (xnums,ynums)=(dict(),dict())
    (xnums['1'],xnums['2'],xnums['3'],xnums['4'],xnums['5'],xnums['6'],xnums['7'],xnums['8'],xnums['9'],xnums['w'],xnums['a'],xnums['s'],xnums['d'])=(1,0,-1,-1,-1,0,1,1,0,0,-1,0,1)
    (ynums['1'],ynums['2'],ynums['3'],ynums['4'],ynums['5'],ynums['6'],ynums['7'],ynums['8'],ynums['9'],ynums['w'],ynums['a'],ynums['s'],ynums['d'])=(-1,-1,-1,0,1,1,1,0,0,1,0,-1,0)
    if player.x+xnums[key]<64 and player.x+xnums[key]>-1:
        player.x+=xnums[key]
    if player.y+ynums[key]<48 and player.y+ynums[key]>-1:
        player.y+=ynums[key]
    move_to(player.shape,(10*player.x+5,10*player.y-5))

def move_robots():
    global robots,player
    for bot in robots:
        if bot.x>player.x:
            bot.x-=1
        elif bot.x<player.x:
            bot.x+=1
        if bot.y>player.y:
            bot.y-=1
        elif bot.y<player.y:
            bot.y+=1
        move_to(bot.shape,(10*bot.x+5,10*bot.y-5))

def check_collisions():
    global finished,robots,player,junk
    surviving_robots=[]
    for bot in robots:
        if collided(bot,junk):
            continue
        jbot=robot_crashed(bot)
        if jbot==False:
            surviving_robots.append(bot)
        else:
            remove_from_screen(jbot.shape)
            jbot.shape = Box((10*jbot.x+5, 10*jbot.y-5), 10, 10, filled=True)
            junk.append(jbot)
    robots=[]
    for bot in surviving_robots:
        if not collided(bot,junk):
            robots.append(bot)
    if robots==[]:
        print('VICTORY!')
        sleep(3)
        finished=True
    if collided(player,robots+junk):
        print('You\'ve been caught!')
        sleep(3)
        finished=True

class Robot:
    pass

class Player:
    pass

def robot_crashed(the_bot):
    global robots
    for a_bot in robots:
        if a_bot==the_bot:
            return False
        if (a_bot.x,a_bot.y)==(the_bot.x,the_bot.y):
            return a_bot
    return False


begin_graphics()
junk=[]
finished = False
numbots=10
place_robots()
safely_place_player()

while not finished:
    move_player()
    move_robots()
    check_collisions()

end_graphics()
