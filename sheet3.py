from gasp import *

begin_graphics()

def draw_face(x,y):

    Circle((x,y),100)
    for xsamp in (x-40,x+40):
        Circle((xsamp,y+40),10)
    Line((x,y+70),(x-20,y+40))
    Line((x-20,y+40),(x+20,y+40))
    Arc((x,y+40),30,225,90)
    Line((x-60,y+60),(x-20,y+70))
    Line((x+60,y+60),(x+20,y+70))
    Circle((x-110,y),10)
    Circle((x+110,y),10)

def derpman(x,y):
    draw_face(x,y)
    Line((x,y-100),(x,y-150))
    Line((x-25,y-125),(x+25,y-125))
    Line((x-25,y-170),(x,y-150))
    Line((x+25,y-170),(x,y-150))

#for i in range(0,400):
#    Line((i,0),(0,400-i))


derpman(300,200)

update_when('key_pressed')


end_graphics()

